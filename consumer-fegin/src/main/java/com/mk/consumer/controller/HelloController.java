package com.mk.consumer.controller;


import com.mk.consumer.feign.HelloFeginClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Autowired
    private HelloFeginClient helloFeginClient;

    @GetMapping(value = "/hello")
    public String sayHi(@RequestParam String name) {
        return "通过fegin客户端调用结果为：" + helloFeginClient.hello(name);
    }
}
