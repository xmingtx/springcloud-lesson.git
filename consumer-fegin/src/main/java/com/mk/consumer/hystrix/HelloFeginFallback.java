package com.mk.consumer.hystrix;

import com.mk.consumer.feign.HelloFeginClient;
import org.springframework.stereotype.Component;

@Component
public class HelloFeginFallback implements HelloFeginClient {

    @Override
    public String hello(String name) {
        return "hello," + name + ",sorry,error!";
    }
}
