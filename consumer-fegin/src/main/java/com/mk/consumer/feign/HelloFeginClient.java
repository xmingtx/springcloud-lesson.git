package com.mk.consumer.feign;

import com.mk.consumer.hystrix.HelloFeginFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "eureka-client-provider", fallback = HelloFeginFallback.class)
public interface HelloFeginClient {

    @GetMapping("/hello")
    String hello(@RequestParam(value = "name") String name);
}
