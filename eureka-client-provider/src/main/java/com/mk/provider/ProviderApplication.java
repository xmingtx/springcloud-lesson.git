package com.mk.provider;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class ProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run( ProviderApplication.class, args );
    }

    @Value("${server.port}")
    String port;

    @GetMapping("/hello")
    public String hello(HttpServletRequest request, @RequestParam String name) {
        System.out.println(request.getHeader("X-Request-Foo"));
        return "hello, " + name + ", 我是服务提供者:端口为：" + port;
    }
}
