package com.mk.ribbon.controller;

import com.mk.ribbon.balancer.LoadBalancer;
import com.mk.ribbon.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import java.util.List;

@RestController
public class HelloController {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LoadBalancer balancer;

    @Autowired
    private HelloService helloService;

//    @GetMapping(value = "/hello")
//    public String hello(@RequestParam String name) {
////        List<ServiceInstance> instances = discoveryClient.getInstances("eureka-client-provider");
////        ServiceInstance instance = instances.get(0);
//        return restTemplate.getForObject("http://eureka-client-provider//hello?name=" + name, String.class);
//
////        List<ServiceInstance> instances = discoveryClient.getInstances("eureka-client-provider");
////        ServiceInstance instance = balancer.getInstanceByCycle(instances);
////        return restTemplate.getForObject(instance.getUri() + "/hello?name=" + name, String.class);
//    }


    @GetMapping(value = "/hello")
    public String hello(@RequestParam String name) {
        return helloService.callHello(name);
    }
}
