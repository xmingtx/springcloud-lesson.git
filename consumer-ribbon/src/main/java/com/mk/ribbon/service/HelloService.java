package com.mk.ribbon.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HelloService {

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "helloFallback")
    public String callHello(String name) {
        return restTemplate.getForObject("http://eureka-client-provider/hello?name="+name,String.class);
    }

    public String helloFallback(String name) {
        return "hello," + name + ",sorry,error!";
    }
}
